package ru.kpfu.itis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.dto.UserDTO;
import ru.kpfu.itis.service.UserService;
import ru.kpfu.itis.util.AuthenticationUtil;

/**
 * Created by Timur on 31.03.2015.
 */
@Controller
public class LoginController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(@RequestParam(required = false) String error,
                        ModelMap map) {
        if (error != null) {
            map.addAttribute("error", "Неправильный логин или пароль");
        }
        map.addAttribute("userDTO", new UserDTO());
//        map.addAttribute("usersCount", userService.countOfUsers());
        if (AuthenticationUtil.isAuthenticated()) {
            return "redirect:/";
        }
        return "loginPage";
    }

}
