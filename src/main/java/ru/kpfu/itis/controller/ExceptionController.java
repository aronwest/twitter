package ru.kpfu.itis.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;
import ru.kpfu.itis.exception.UserNotFoundException;
import ru.kpfu.itis.util.Constants;

/**
 * Created by timur on 19.04.15.
 */
@ControllerAdvice
public class ExceptionController {

    @ModelAttribute("defPic")
    public String defPic() {
        return Constants.DEF_PIC;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({NoHandlerFoundException.class, NoSuchRequestHandlingMethodException.class})
    public String error404(Exception e) {
        return "error/404";
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(UserNotFoundException.class)
    public ModelAndView userNotFound(UserNotFoundException e) {
        //        modelAndView.addObject("login", e.getLogin());
        return new ModelAndView("error/usernotfound", "login", e.getLogin());
    }
}
