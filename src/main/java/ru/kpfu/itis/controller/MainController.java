package ru.kpfu.itis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.exception.UserNotFoundException;
import ru.kpfu.itis.model.Retweet;
import ru.kpfu.itis.model.Tweet;
import ru.kpfu.itis.model.User;
import ru.kpfu.itis.service.TweetService;
import ru.kpfu.itis.service.UserService;

import java.util.Set;
import java.util.concurrent.ExecutionException;

import static java.util.Collections.singletonList;
import static ru.kpfu.itis.util.AuthenticationUtil.getAuthUser;


/**
 * Created by Dilya on 18.03.2015.
 */
@Controller
public class MainController {

    @Autowired
    UserService userService;
    @Autowired
    TweetService tweetService;
    @Autowired
    private Sort sortingStrategy;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String userTape(ModelMap map) {
        map.addAttribute("tweets",
                tweetService.tweetsAndRetweetsAccessedTo(getAuthUser(), 0, 0));
        return "tape/tape";
    }

    @RequestMapping(value = "/ajax/tweet/more", method = RequestMethod.POST)
    public String getMoreTweets(
            @RequestParam("tweetNum") int tweetNum,
            @RequestParam("retweetNum") int retweetNum,
            ModelMap map) {
        map.addAttribute("tweets",
                tweetService.tweetsAndRetweetsAccessedTo(getAuthUser(), tweetNum, retweetNum));
        return "postsInclude";
    }

    @RequestMapping(value = "/{login:[A-Za-z0-9]+}")
    public String userProfile(@PathVariable("login") User user, @PathVariable String login, ModelMap map) throws ExecutionException, InterruptedException {
        if (user == null) throw new UserNotFoundException(login);
        map.addAttribute(user).addAttribute("tweets",
                tweetService.findByCreator(user, sortingStrategy));
        return "profile/profile";
    }

    @RequestMapping(value = "/tweet/{tweet_id:[1-9]+\\d*}")
    public String tweetWithAnswers(@PathVariable("tweet_id") Tweet tweet, ModelMap map) {
        map.addAttribute(tweet);
        return "tweetPage/tweet";
    }

    @ResponseBody
    @RequestMapping(value = "/ajax/tweet/edit", method = RequestMethod.POST)
    public Object editTweet(@RequestParam("tweetId") Tweet tweet,
                            @RequestParam("tweetText") String tweetText) {
        final Tweet updatedTweet = tweetService.update(tweet, tweetText);
        return new Object() {
            public String text = updatedTweet.getText();
            public int mentionedCount;

            {
                Set<User> mentionedUsers = updatedTweet.getMentionedUsers();
                mentionedCount = mentionedUsers == null ? 0 : mentionedUsers.size();
            }
        };
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/ajax/tweet/delete", method = RequestMethod.POST)
    public void deleteTweet(@RequestParam("tweetId") Long tweetId) {
        tweetService.deleteTweet(tweetId);
    }

    @RequestMapping(value = "/ajax/retweet/add", method = RequestMethod.POST)
    public String addRetweet(@RequestBody Retweet retweet, ModelMap map) {
        map.addAttribute("tweets", singletonList(tweetService.save(retweet)));
        return "postsInclude";
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/ajax/retweet/delete", method = RequestMethod.POST)
    public void deleteRetweet(@RequestParam("tweetId") Long tweetId) {
        tweetService.deleteRetweet(tweetId);
    }

    @RequestMapping(value = "/favourites", method = RequestMethod.GET)
    public String userFavourites(ModelMap map) {
        map.addAttribute("tweets", tweetService.getFavourites());
        return "favourites/favourites";
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/ajax/favourites/add", method = RequestMethod.POST)
    public void addToFavourites(@RequestParam("tweetId") long tweetId) {
        tweetService.addToFavourites(tweetId);
    }
}
