package ru.kpfu.itis.model.enums;

/**
 * Created by Timur on 31.03.2015.
 */
public enum UserGroup {

    ADMIN("Администратор", 1), USER("Пользователь", 2);

    private final String groupName;
    private final int accessLevel;

    UserGroup(String groupName, int accessLevel) {
        this.groupName = groupName;
        this.accessLevel = accessLevel;
    }

    public String getGroupName() {
        return groupName;
    }

    public int getAccessLevel() {
        return accessLevel;
    }
}
