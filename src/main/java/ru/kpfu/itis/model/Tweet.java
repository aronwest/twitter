package ru.kpfu.itis.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.Objects;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.util.Set;

import static com.google.common.base.Objects.equal;

/**
 * Created by Timur on 05.03.2015.
 */
@Entity
@Table(name = "tweets")
@JsonIgnoreProperties({
        "answers",
        "retweets",
        "whoAddedToFavourites",
        "mentionedUsers",
        "accessibleTo"
})
public class Tweet extends AbstractTweet {

    @Id
    @Column(name = "tweet_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long tweetId;

    @ManyToOne(optional = false)
    @JoinColumn(name = "creator", nullable = false)
    private User creator;

    @NotBlank
    @Column
    private String text;

    @Column
    private String coordinates;

    @OrderBy("time asc")
    @OneToMany(mappedBy = "mainTweet", fetch = FetchType.EAGER)
    private Set<Tweet> answers;

    @ManyToOne
    @JoinColumn(name = "main_tweet_id")
    private Tweet mainTweet;

    @OneToMany(mappedBy = "tweet", fetch = FetchType.EAGER)
    private Set<Retweet> retweets;

    @ManyToMany(mappedBy = "favourites", fetch = FetchType.EAGER)
    private Set<User> whoAddedToFavourites;

    @ManyToMany(mappedBy = "mentions", fetch = FetchType.EAGER)
    private Set<User> mentionedUsers;

    @ManyToMany(mappedBy = "accessibleTweets", fetch = FetchType.EAGER)
    private Set<User> accessibleTo;

    public Long getTweetId() {
        return tweetId;
    }

    public void setTweetId(Long tweetId) {
        this.tweetId = tweetId;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public Tweet getMainTweet() {
        return mainTweet;
    }

    public void setMainTweet(Tweet mainTweet) {
        this.mainTweet = mainTweet;
    }

    public Set<Tweet> getAnswers() {
        return answers;
    }

    public void setAnswers(Set<Tweet> answers) {
        this.answers = answers;
    }

    public Set<User> getMentionedUsers() {
        return mentionedUsers;
    }

    public void setMentionedUsers(Set<User> mentionedUsers) {
        this.mentionedUsers = mentionedUsers;
    }

    public Set<User> getWhoAddedToFavourites() {
        return whoAddedToFavourites;
    }

    public void setWhoAddedToFavourites(Set<User> whoAddedToFavourites) {
        this.whoAddedToFavourites = whoAddedToFavourites;
    }

    public Set<Retweet> getRetweets() {
        return retweets;
    }

    public void setRetweets(Set<Retweet> retweets) {
        this.retweets = retweets;
    }

    public Set<User> getAccessibleTo() {
        return accessibleTo;
    }

    public void setAccessibleTo(Set<User> accessibleTo) {
        this.accessibleTo = accessibleTo;
    }

    @Override
    public String toString() {
        return "Tweet{" +
                "tweet_id=" + tweetId +
                ", creator=" + creator +
                ", text='" + text + '\'' +
                ", time=" + time +
                ((timeView == null || timeView.equals("")) ? "" : (", timeView=" + timeView)) +
                ", coordinates='" + coordinates + '\'' +
                (mainTweet == null ? "" : (", mainTweetId=" + mainTweet.tweetId)) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tweet tweet = (Tweet) o;
        return equal(tweetId, tweet.tweetId) &&
                equal(creator, tweet.creator) &&
                equal(text, tweet.text) &&
                equal(time, tweet.time) &&
                equal(coordinates, tweet.coordinates);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(tweetId, creator, text, time, coordinates);
    }
}
