package ru.kpfu.itis.model;

import com.google.common.base.Objects;

import java.io.Serializable;

/**
 * Created by timur on 02.05.15.
 */
public class RetweetId implements Serializable {

    private Long tweetId;

    private String creator;

    public RetweetId() {
    }

    public RetweetId(Long tweetId, String creator) {

        this.tweetId = tweetId;
        this.creator = creator;
    }

    public Long getTweetId() {
        return tweetId;
    }

    public void setTweetId(Long tweetId) {
        this.tweetId = tweetId;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RetweetId retweetId = (RetweetId) o;
        return Objects.equal(tweetId, retweetId.tweetId) &&
                Objects.equal(creator, retweetId.creator);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(tweetId, creator);
    }
}
