package ru.kpfu.itis.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.Objects;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.URL;
import ru.kpfu.itis.model.enums.UserGroup;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Set;

import static com.google.common.base.MoreObjects.toStringHelper;
import static com.google.common.base.Objects.equal;

/**
 * Created by Timur on 02.03.2015.
 */
@Entity
@Table(name = "users")
@JsonIgnoreProperties({
        "password",
        "salt",
        "userGroup",
        "tweets",
        "retweets",
        "favourites",
        "mentions",
        "accessibleTweets"
})
public class User implements Serializable {

    //    @Column(name = ) - не указываем, так как по умолчанию совпадает с названием поля
    @Id
    @Column
    private String login;

    @Column
    private String password;

    @Column
    private String salt;

    @Column
    private String firstname;

    @Column
    private String lastname;

    @Column
    @Email
    private String email;

    @Column
    @Pattern(regexp = "[+]?[0-9-]+", message = "must be a valid phone number")
    private String phone;

    @Column
    @URL
    private String facebook;

    @Column
    @URL
    private String vk;

    @Column
    private String instagram;

    @Column
    private String skype;

    @Column(name = "avatar_url")
    private String avatarURL;

    @Column
    private Long birthdate;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "user_group")
    private UserGroup userGroup = UserGroup.USER;

    @OneToMany(mappedBy = "creator", cascade = {CascadeType.REMOVE})
    private Set<Tweet> tweets;

    @OneToMany(mappedBy = "retweeter", cascade = {CascadeType.REMOVE})
    private Set<Retweet> retweets;

    @ManyToMany
    @JoinTable(
            name = "favourites",
            joinColumns = {
                    @JoinColumn(name = "login", nullable = false)
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "tweet_id", nullable = false)
            }
    )
    private Set<Tweet> favourites;

    @ManyToMany
    @JoinTable(
            name = "mentions",
            joinColumns = {
                    @JoinColumn(name = "login", nullable = false)
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "tweet_id", nullable = false)
            }
    )
    private Set<Tweet> mentions;

    @ManyToMany
    @JoinTable(
            name = "access",
            joinColumns = {
                    @JoinColumn(name = "accessible_to", nullable = false)
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "tweet_id", nullable = false)
            }
    )
    private Set<Tweet> accessibleTweets;

    public UserGroup getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(UserGroup userGroup) {
        this.userGroup = userGroup;
    }

    public Long getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Long birthdate) {
        this.birthdate = birthdate;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getVk() {
        return vk;
    }

    public void setVk(String vk) {
        this.vk = vk;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public Set<Tweet> getTweets() {
        return tweets;
    }

    public void setTweets(Set<Tweet> tweets) {
        this.tweets = tweets;
    }

    public Set<Tweet> getMentions() {
        return mentions;
    }

    public void setMentions(Set<Tweet> mentions) {
        this.mentions = mentions;
    }

    public Set<Retweet> getRetweets() {
        return retweets;
    }

    public void setRetweets(Set<Retweet> retweets) {
        this.retweets = retweets;
    }

    public Set<Tweet> getFavourites() {
        return favourites;
    }

    public void setFavourites(Set<Tweet> favourites) {
        this.favourites = favourites;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public void setAvatarURL(String avatarURL) {
        this.avatarURL = avatarURL;
    }

    public Set<Tweet> getAccessibleTweets() {
        return accessibleTweets;
    }

    public void setAccessibleTweets(Set<Tweet> accessibleTweets) {
        this.accessibleTweets = accessibleTweets;
    }

    @Override
    public String toString() {
        return toStringHelper(this)
                .add("login", login)
                .add("password", password)
                .add("salt", salt)
                .add("firstname", firstname)
                .add("lastname", lastname)
                .add("email", email)
                .add("phone", phone)
                .add("facebook", facebook)
                .add("vk", vk)
                .add("instagram", instagram)
                .add("skype", skype)
                .add("avatarURL", avatarURL)
                .add("birthdate", birthdate)
                .add("userGroup", userGroup)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return equal(login, user.login) &&
                equal(password, user.password) &&
                equal(salt, user.salt) &&
                equal(firstname, user.firstname) &&
                equal(lastname, user.lastname) &&
                equal(email, user.email);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(login, password, salt, firstname, lastname, email);
    }
}
