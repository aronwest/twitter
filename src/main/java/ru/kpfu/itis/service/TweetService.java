package ru.kpfu.itis.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import ru.kpfu.itis.dto.TweetDTO;
import ru.kpfu.itis.model.AbstractTweet;
import ru.kpfu.itis.model.Retweet;
import ru.kpfu.itis.model.Tweet;
import ru.kpfu.itis.model.User;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by Timur on 06.04.2015.
 */
public interface TweetService {

    List<AbstractTweet> findByCreator(User creator, Sort sort) throws InterruptedException, ExecutionException;

    Tweet save(Tweet tweet);

    Tweet save(TweetDTO tweetDTO);

    Retweet save(Retweet retweet);

    void addToFavourites(long tweetId);

    void deleteTweet(Long tweetId);

    void deleteRetweet(Long tweetId);

    void update(Long id, String text);

    Tweet update(Tweet tweet, String newText);

    List<Tweet> getFavourites();

    Page<Tweet> getTweetsPage(Pageable pageable);

    Tweet findById(Long id);

    Page<Tweet> findTweetsAccessedTo(User user, Pageable pageable);

    List<AbstractTweet> tweetsAndRetweetsAccessedTo(User user, int tweetNum, int retweetNum);
}
