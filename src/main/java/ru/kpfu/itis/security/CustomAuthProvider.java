package ru.kpfu.itis.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.model.User;
import ru.kpfu.itis.service.UserService;
import ru.kpfu.itis.util.PassEncryptionUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ArthurRec on 16.03.2015.
 */

@Component
public class CustomAuthProvider implements AuthenticationProvider {

    @Autowired
    UserService userService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

//        PreAuthenticatedAuthenticationToken - для токен based нужен
        String login = authentication.getName();
        String password = authentication.getCredentials().toString();
        User user = userService.findByLogin(login);
        if (user == null) {
            throw new UsernameNotFoundException("Incorrect login");
        }
        if (PassEncryptionUtil.encrypt(password, user.getSalt()).equals(user.getPassword())) {
            List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
            if (user.getUserGroup() != null) {
                grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_" + user.getUserGroup()));
            } else {
                grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
            }
            return new UsernamePasswordAuthenticationToken(user, null, grantedAuthorities);
        } else {
            throw new BadCredentialsException("Invalid password");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}