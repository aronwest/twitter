package ru.kpfu.itis.annotation;

/**
 * Created by Timur on 02.04.2015.
 */

/**
 * Levels of logging
 */
public enum LogLevel {
    /**
     * Tracing, entry and exit from a method, method arguments
     */
    TRACE,

    /**
     * Debug messages
     */
    DEBUG,

    /**
     * Normal application work: app initialization, different actions
     */
    INFO,

    /**
     * Actions and circumstances that may lead to an unexpected action
     */
    WARN,

    /**
     * Errors
     */
    ERROR
}
