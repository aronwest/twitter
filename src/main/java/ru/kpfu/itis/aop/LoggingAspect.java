package ru.kpfu.itis.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.annotation.Loggable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

/**
 * Created by Timur on 02.04.2015.
 */
@Component
@Aspect
public class LoggingAspect {

    @Pointcut(value = "@annotation(loggable)", argNames = "loggable")
    public void loggablePoint(Loggable loggable) {
    }

    @Before(value = "loggablePoint(loggable)", argNames = "joinPoint,loggable")
    public void before(JoinPoint joinPoint, Loggable loggable) {
        Class loggableClass = joinPoint.getTarget().getClass();
        Logger logger = LoggerFactory.getLogger(loggableClass);
        Object[] arguments = joinPoint.getArgs();
        String loggableMethodName = joinPoint.getSignature().getName();
        Method loggingMethod;
        try {
            loggingMethod = getLoggingMethod(logger, loggable);
            loggingMethod.invoke(logger,
                    "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^START^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^", null);
            if (arguments.length == 0) {
                loggingMethod.invoke(logger, "METHOD \"{}\" HAS NOT ANY ARGUMENTS", new Object[]{loggableMethodName});
            } else {
                loggingMethod.invoke(logger, "METHOD \"{}\" ARGUMENTS:", new Object[]{loggableMethodName});
                logArguments(logger, loggingMethod, arguments);
            }
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            logger.error("Exception from before aspect : {}", e.getMessage());
        }

    }

    @AfterReturning(
            pointcut = "loggablePoint(loggable)",
            returning = "returnValue",
            argNames = "joinPoint,loggable,returnValue")
    public void afterReturning(JoinPoint joinPoint, Loggable loggable, Object returnValue) {
        Class loggableClass = joinPoint.getTarget().getClass();
        Logger logger = LoggerFactory.getLogger(loggableClass);
        Object[] arguments = joinPoint.getArgs();
        String loggableMethodName = joinPoint.getSignature().getName();
        Method loggingMethod;
        try {
            loggingMethod = getLoggingMethod(logger, loggable);
            if (arguments.length != 0) {
                loggingMethod.invoke(logger, "METHOD \"{}\" ARGUMENTS AFTER RETURNING: ", new Object[]{loggableMethodName});
                logArguments(logger, loggingMethod, arguments);
            }
            loggingMethod.invoke(logger, "METHOD \"{}\" RETURNED VALUE: {}",
                    new Object[]{
                            loggableMethodName,
                            returnValue
                    });
            loggingMethod.invoke(logger,
                    "==============================================================END===============================================================", null);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            logger.error("Exception from afterReturning aspect : {}", e.getMessage());
        }
    }

    @AfterThrowing(pointcut = "loggablePoint(loggable)", argNames = "joinPoint,loggable,throwable", throwing = "throwable")
    public void afterThrowing(JoinPoint joinPoint, Loggable loggable, Throwable throwable) {
        Class loggableClass = joinPoint.getTarget().getClass();
        Logger logger = LoggerFactory.getLogger(loggableClass);
        logger.error("METHOD \"{}\" THREW EXCEPTION: ", joinPoint.getSignature().getName());
        for (StackTraceElement stackTraceElement : throwable.getStackTrace()) {
            logger.error("   " + stackTraceElement.toString());
        }
        logger.error("==============================================================END===============================================================");
    }

    private void logArguments(
            Logger logger,
            Method loggingMethod,
            Object[] arguments) throws InvocationTargetException, IllegalAccessException {
        for (int i = 0; i < arguments.length; i++) {
            if (arguments[i] instanceof Collection) {
                Collection arg = (Collection) arguments[i];
                loggingMethod
                        .invoke(logger, "---ARG #{} IS COLLECTION: {}",
                                new Object[]{
                                        i,
                                        Arrays.toString((arg).toArray())
                                });
            } else if (arguments[i] instanceof Map) {
                Map arg = (Map) arguments[i];
                if (arg.isEmpty()) {
                    loggingMethod.invoke(logger, "---ARG #{} IS EMPTY MAP", new Object[]{i});
                } else {
                    loggingMethod
                            .invoke(logger, "---ARG #{} IS MAP:", new Object[]{i});
                    for (Object key : arg.keySet()) {
                        loggingMethod
                                .invoke(logger, "------{} : {}", new Object[]{key, arg.get(key)});
                    }
                }
            } else {
                loggingMethod
                        .invoke(logger, "---ARG #{}: {}",
                                new Object[]{
                                        i,
                                        arguments[i]
                                });
            }
        }
    }

    private Method getLoggingMethod(Logger logger, Loggable loggable) throws NoSuchMethodException {
        return logger
                .getClass()
                .getMethod(
                        //Method name
                        loggable.value().toString().toLowerCase(),
                        //Types of method arguments
                        String.class, Object[].class);
    }

}
