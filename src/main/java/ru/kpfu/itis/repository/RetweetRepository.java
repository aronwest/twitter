package ru.kpfu.itis.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.kpfu.itis.model.Retweet;
import ru.kpfu.itis.model.RetweetId;
import ru.kpfu.itis.model.User;

import java.util.List;

/**
 * Created by timur on 02.05.15.
 */
public interface RetweetRepository extends AbstractTweetRepository<Retweet, RetweetId>, RetweetRepositoryCustom {

    @Query("select rets from Retweet rets where " +
            "rets.tweetId in (select usrTws.tweetId from User u join u.retweets usrTws where u=:user and usrTws.tweet.mainTweet is null) " +
            "or rets.tweetId in (select acc.tweetId from User u join u.accessibleTweets acc where u=:user and acc.mainTweet is null)" +
            "or rets.tweetId in (select pubTws.tweetId from Tweet pubTws where pubTws.accessibleTo is empty and pubTws.mainTweet is null)")
    Page<Retweet> findRetweetsAccessedTo(@Param("user") User user, Pageable pageable);

    List<Retweet> findByCreatorAndTweet_MainTweetIsNull(String creator, Sort sort);

    @Query("select rets from Retweet rets where rets.creator=:retweeter and rets.tweet.accessibleTo is empty and rets.tweet.mainTweet is null")
    List<Retweet> findPubRetweets(@Param("retweeter") String retweeter, Sort sort);

    @Query("select rets from Retweet rets join fetch rets.tweet original " +
            "where rets.creator=:retweeter " +
            "and (original.accessibleTo is empty or :user in (select t.login from original.accessibleTo t)) " +
            "and rets.tweet.mainTweet is null")
    List<Retweet> findByRetweeterAccessedTo(@Param("retweeter") String retweeter, @Param("user") String login, Sort sort);

}
