package ru.kpfu.itis.repository;

import ru.kpfu.itis.model.Retweet;
import ru.kpfu.itis.model.User;

import java.util.List;

/**
 * Created by timur on 02.05.15.
 */
public interface RetweetRepositoryCustom {

    List<Retweet> findRetweetsAccessedTo(User user, int offset, int maxResult);

}
