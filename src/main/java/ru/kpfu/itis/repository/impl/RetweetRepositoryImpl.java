package ru.kpfu.itis.repository.impl;

import ru.kpfu.itis.model.Retweet;
import ru.kpfu.itis.model.User;
import ru.kpfu.itis.repository.RetweetRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by timur on 02.05.15.
 */
public class RetweetRepositoryImpl implements RetweetRepositoryCustom {

    @PersistenceContext
    EntityManager em;

    @Override
    public List<Retweet> findRetweetsAccessedTo(User user, int offset, int maxResult) {
        String idsOfAccessedTweets = "select acc.tweetId from User u join u.accessibleTweets acc where u.login=:login and acc.mainTweet is null";
        String idsOfUsersRetweets = "select usrTws.tweet.tweetId from User u join u.retweets usrTws where u.login=:login and usrTws.tweet.mainTweet is null";
        String idsOfPublicTweets = "select pubTws.tweetId from Tweet pubTws where pubTws.accessibleTo is empty and pubTws.mainTweet is null";
        TypedQuery<Retweet> query = em.createQuery("select rets from Retweet rets where " +
                "rets.tweetId in (" + idsOfAccessedTweets + ") " +
                "or rets.tweetId in (" + idsOfUsersRetweets + ") " +
                "or rets.tweetId in (" + idsOfPublicTweets + ") " +
                "order by rets.time desc", Retweet.class)
                .setParameter("login", user.getLogin())
                .setFirstResult(offset)
                .setMaxResults(maxResult);
        return query.getResultList();
    }
}
