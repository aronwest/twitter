package ru.kpfu.itis.repository.impl;

import ru.kpfu.itis.model.Tweet;
import ru.kpfu.itis.model.User;
import ru.kpfu.itis.repository.TweetRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by timur on 02.05.15.
 */
public class TweetRepositoryImpl implements TweetRepositoryCustom {

    @PersistenceContext
    EntityManager em;

    @Override
    public List<Tweet> findTweetsAccessedTo(User user, int offset, int maxResult) {
//        CriteriaBuilder builder = em.getCriteriaBuilder();
//        CriteriaQuery<Tweet> query = builder.createQuery(Tweet.class);
//        Root<Tweet> root = query.from(Tweet.class);
//        List<Predicate> predicates = new ArrayList<>();
        String idsOfAccessedTweets = "select tws.tweetId from User u join u.accessibleTweets tws where u.login=:login and tws.mainTweet is null";
        String idsOfUsersTweets = "select tws.tweetId from User u join u.tweets tws where u.login=:login and tws.mainTweet is null";
        String idsOfPublicTweets = "select tws.tweetId from Tweet tws where tws.accessibleTo is empty and tws.mainTweet is null";
        TypedQuery<Tweet> query = em.createQuery("select t from Tweet t where " +
                "t.tweetId in (" + idsOfAccessedTweets + ") " +
                "or t.tweetId in (" + idsOfUsersTweets + ") " +
                "or t.tweetId in (" + idsOfPublicTweets + ") " +
                "order by t.time desc ", Tweet.class)
                .setParameter("login", user.getLogin())
                .setFirstResult(offset)
                .setMaxResults(maxResult);
        return query.getResultList();
    }
}
