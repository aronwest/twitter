package ru.kpfu.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.model.User;

import java.util.Collection;
import java.util.Set;

/**
 * Created by Timur on 05.04.2015.
 */
public interface UserRepository extends JpaRepository<User, String> {

    User findByLoginIgnoreCase(String login);

    Set<User> findByLoginIn(Collection<String> logins);

}
