package ru.kpfu.itis.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.kpfu.itis.model.Tweet;
import ru.kpfu.itis.model.User;

import java.util.List;

/**
 * Created by Timur on 05.04.2015.
 */
public interface TweetRepository extends AbstractTweetRepository<Tweet, Long>, TweetRepositoryCustom {

    List<Tweet> findByCreatorAndMainTweetIsNull(User creator, Sort sort);

    Page<Tweet> findByMainTweetIsNull(Pageable pageable);

    @Query("select t from Tweet t where " +
            "t.tweetId in (select accTws.tweetId from User u join u.accessibleTweets accTws where u=:user and accTws.mainTweet is null) " +
            "or t.tweetId in (select usersTws.tweetId from User u join u.tweets usersTws where u=:user and usersTws.mainTweet is null) " +
            "or t.tweetId in (select pubTws.tweetId from Tweet pubTws where pubTws.accessibleTo is empty and pubTws.mainTweet is null)")
    Page<Tweet> findTweetsAccessedTo(@Param("user") User user, Pageable pageable);

    List<Tweet> findByCreatorAndMainTweetIsNullAndAccessibleToIsNull(User user, Sort sort);

    @Query("select t from Tweet t where t.creator.login=:creator " +
            "and (:user in (select acc.login from t.accessibleTo acc) or t.accessibleTo is empty) " +
            "and t.mainTweet is null ")
    List<Tweet> findByCreatorAccessedTo(@Param("creator") String creator, @Param("user") String login, Sort sort);

    @Query("select fav from User u join u.favourites fav where u.login=:login and fav.mainTweet is null")
    List<Tweet> findFavouritesByUser(@Param("login") String login);

    @Modifying
    @Query(nativeQuery = true, value = "DELETE FROM mentions WHERE tweet_id=:tweetId")
    void deleteMentions(@Param("tweetId") Long tweetId);

    @Modifying
    @Query("update Tweet t set t.text=:text where t.tweetId=:id")
    void updateTweetText(@Param("id") Long id, @Param("text") String text);

    @Modifying
    @Query(nativeQuery = true, value = "INSERT INTO mentions(tweet_id, login) VALUES (:tweetId,:login)")
    void addMention(@Param("tweetId") Long tweetId, @Param("login") String login);

    @Modifying
    @Query(nativeQuery = true, value = "INSERT INTO favourites(tweet_id, login) VALUES (:tweetId,:login)")
    void addToFavourites(@Param("tweetId") Long tweetId, @Param("login") String login);

}
