package ru.kpfu.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.kpfu.itis.model.AbstractTweet;

import java.io.Serializable;

/**
 * Created by timur on 02.05.15.
 */
@NoRepositoryBean
public interface AbstractTweetRepository<T extends AbstractTweet, ID extends Serializable> extends JpaRepository<T, ID> {

}
