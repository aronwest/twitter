package ru.kpfu.itis.util;

import ru.kpfu.itis.dto.TweetDTO;
import ru.kpfu.itis.dto.UserDTO;
import ru.kpfu.itis.model.Tweet;
import ru.kpfu.itis.model.User;

/**
 * Created by ArthurRec on 16.03.2015.
 */
public class FormMapper {

    /**
     * Converts User form to User model;
     *
     * @param userDTO user Data Transfer Object
     * @return generated User Object
     */
    public static User userFormToUser(UserDTO userDTO) {
        User user = new User();
        user.setLogin(userDTO.getLogin());
        user.setFirstname(userDTO.getFirstname());
        user.setLastname(userDTO.getLastname());
        user.setEmail(userDTO.getEmail());
        return user;

    }

    public static Tweet tweetFormToTweet(TweetDTO tweetDTO, User creator, Tweet mainTweet) {
        // tweet_id,creator,text,time,coordinates,main_tweet_id
        Tweet tweet = new Tweet();
        tweet.setTweetId(tweetDTO.getTweetId());
        tweet.setCreator(creator);
        tweet.setText(tweetDTO.getText());
        tweet.setCoordinates(tweetDTO.getCoordinates());
        tweet.setMainTweet(mainTweet);
        return tweet;
    }

    // TODO tweetToTweetForm()

}
