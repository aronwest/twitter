package ru.kpfu.itis.util;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.UUID;


/**
 * Created by ArthurRec on 16.03.2015.
 */
public class PassEncryptionUtil {

    private static String mergePassSalt(String password, String salt) {
        return password + salt;
    }

    /**
     * Generate hash for users with own password
     */
    public static String encrypt(String unencrypted, String salt) {
        return DigestUtils.md5Hex(mergePassSalt(unencrypted, salt));
    }

    public static String generateSalt() {
        return UUID.randomUUID().toString();
    }

}
