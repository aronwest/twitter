package ru.kpfu.itis.util;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.kpfu.itis.model.User;

import java.util.Collection;

/**
 * Created by ArthurRec on 16.03.2015.
 */
public class AuthenticationUtil {

    /**
     * @param authentication
     * @return true in two cases - if the auth is NULL or if it's not AuthToken;
     */
    private static boolean isNotAuth(Authentication authentication) {
        return authentication == null || !(authentication instanceof UsernamePasswordAuthenticationToken);
    }

    public static User getAuthUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return isNotAuth(authentication) ? null : (User) authentication.getPrincipal();
    }

    public static Collection<? extends GrantedAuthority> getAuthUserRoles() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return isNotAuth(authentication) ? null : authentication.getAuthorities();
    }

    // login = id;
    // EXAMPLE:
    // String login = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getLogin();

    public static String getAuthUserLogin() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return isNotAuth(authentication) ? null : ((User) authentication.getPrincipal()).getLogin();
    }

    public static boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return !isNotAuth(authentication);
    }

    public static void updatePrincipalData(User user) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(
                        user, null, authentication.getAuthorities());
        token.setDetails(authentication.getDetails());
        securityContext.setAuthentication(token);
    }

}
