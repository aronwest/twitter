package ru.kpfu.itis.dto;

import com.google.common.base.MoreObjects;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by ArthurRec on 08.04.2015.
 */
public class TweetDTO {

    private Long tweetId;
    @NotBlank
    @Size(min = 1, max = 140)
    private String text;
    private String coordinates;
    private Long mainTweetId;

    private List<String> accessibleTo;

    public Long getTweetId() {
        return tweetId;
    }

    public void setTweetId(Long tweetId) {
        this.tweetId = tweetId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public Long getMainTweetId() {
        return mainTweetId;
    }

    public void setMainTweetId(Long mainTweetId) {
        this.mainTweetId = mainTweetId;
    }


    public List<String> getAccessibleTo() {
        return accessibleTo;
    }

    public void setAccessibleTo(List<String> accessibleTo) {
        this.accessibleTo = accessibleTo;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("tweetId", tweetId)
                .add("text", text)
                .add("coordinates", coordinates)
                .add("mainTweetId", mainTweetId)
                .add("accessibleTo", accessibleTo)
                .toString();
    }
}
