package ru.kpfu.itis.dto;

import com.google.common.base.MoreObjects;
import ru.kpfu.itis.model.enums.UserGroup;

/**
 * Created by ArthurRec on 16.03.2015.
 */
public class UserDTO {

    private String login;
    private String password;
    private String repassword;
    private String firstname;
    private String lastname;
    private String email;
    private String phone;
    private String facebook;
    private String vk;
    private String instagram;
    private String skype;
    private String avatarURL;
    private String birthdate;
    private UserGroup userGroup;

    public String getAvatarURL() {
        return avatarURL;
    }

    public void setAvatarURL(String avatarURL) {
        this.avatarURL = avatarURL;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public UserGroup getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(UserGroup userGroup) {
        this.userGroup = userGroup;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getVk() {
        return vk;
    }

    public void setVk(String vk) {
        this.vk = vk;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getRepassword() {
        return repassword;
    }

    public void setRepassword(String repassword) {
        this.repassword = repassword;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("login", login)
                .add("password", password)
                .add("repassword", repassword)
                .add("firstname", firstname)
                .add("lastname", lastname)
                .add("email", email)
                .add("phone", phone)
                .add("facebook", facebook)
                .add("vk", vk)
                .add("instagram", instagram)
                .add("skype", skype)
                .add("avatarURL", avatarURL)
                .add("birthdate", birthdate)
                .add("userGroup", userGroup)
                .omitNullValues()
                .toString();
    }
}
