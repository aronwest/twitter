package ru.kpfu.itis.dto;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Dilya on 08.04.2015.
 */
public class AvatarDTO {

    MultipartFile file;
    String login;

    public AvatarDTO() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }


}
