import groovy.transform.Field

@Field def random = new Random()
def csvDirectory = new File('db/changelog/csv2')
def reader = System.in.newReader();
def inputedLine;
int startId = 100;
print 'users count (default 100): '
int usersCount = (inputedLine = reader.readLine()).isInteger() ? inputedLine.toInteger() : {
    println 'incorrect number, using default value 100'
    100
}()
print 'tweets count (default 10000): '
int tweetsCount = (inputedLine = reader.readLine()).isInteger() ? inputedLine.toInteger() : {
    println 'incorrect number, using default value 10000'
    10000
}()
print 'retweets count (default 10000): '
int retweetsCount = (inputedLine = reader.readLine()).isInteger() ? inputedLine.toInteger() : {
    println 'incorrect number, using default value 10000'
    10000
}()
print 'favourites count (default 1000): '
int favouritesCount = (inputedLine = reader.readLine()).isInteger() ? inputedLine.toInteger() : {
    println 'incorrect number, using default value 1000'
    1000
}()
reader.close()
def startTime = System.currentTimeMillis()
println 'creation started...'
csvDirectory.mkdir()

def randomInt = {
    int maxValue = Integer.MAX_VALUE,
    boolean plusStartId = true
        ->
        (maxValue != 0 ? random.nextInt(maxValue) : 0) + (plusStartId ? startId : 0)
}

def csvFiles = [
        usersData     : new File(csvDirectory, 'usersData.csv'),
        tweetsData    : new File(csvDirectory, 'tweetsData.csv'),
        retweetsData  : new File(csvDirectory, 'retweetsData.csv'),
        mentionsData  : new File(csvDirectory, 'mentionsData.csv'),
        favouritesData: new File(csvDirectory, 'favouritesData.csv')
]
csvFiles['usersData'].text = 'login,password,salt,firstname,lastname,email,birthdate\n'
csvFiles['tweetsData'].text = 'tweet_id,creator,text,time,coordinates,main_tweet_id\n'
csvFiles['mentionsData'].text = 'tweet_id,login\n'
csvFiles['favouritesData'].text = 'tweet_id,login\n'
csvFiles['retweetsData'].text = 'tweet_id,time,retweeter\n'


def usersDataThread = Thread.start('userData generation') {
    println '\'usersData.csv\' data filling started...'
    usersCount.times {
        csvFiles['usersData'].append "login$it," +
                "87ff0f150874bf4ba0e88d60b3b40623," +
                "96bbcb1c-66a7-47b3-b160-e6f3dd9cc576," +
                "пользователь $it," +
                "пользователич $it," +
                "user$it@gmail.com," +
                "${randomInt()}\n"
    }
    println '\'usersData.csv\' data filling ended'
}

def tweetsAndMentionsDataThread = Thread.start {
    int randomCreator, randomMention1, randomMention2, randomMainTweetId
    boolean itHasMainTweet
    String lineToWrite
    println '\'tweetsData.csv\' and \'mentions.csv\' data filling started...'
    tweetsCount.times {
        itHasMainTweet = randomInt(100, false) < 60 && it >= 5
        lineToWrite = "${it + 100}," +
                "login${randomCreator = randomInt usersCount, false},"
        if (itHasMainTweet) {
            lineToWrite += "ответ ПОЛЬЗОВАТЕЛЯ login$randomCreator " +
                    "на твит ${(randomMainTweetId = randomInt(it)) != (it + 100) ? randomMainTweetId : --randomMainTweetId}"
        } else {
            lineToWrite += "твит ПОЛЬЗОВАТЕЛЯ login$randomCreator "
        }
        randomMention1 = randomInt(usersCount, false)
        while ((randomMention2 = randomInt(usersCount, false)) == randomMention1) {
            randomMention2 = randomInt(usersCount, false);
        }
        lineToWrite += "c упоминанием пользователя <a href=\"/login${randomMention1}\">@login$randomMention1</a> " +
                "и пользователя <a href=\"/login${randomMention2 = randomInt usersCount, false}\">@login$randomMention2</a>," +
                "${randomInt()}," +
                "${randomInt()}," +
                "${itHasMainTweet ? randomMainTweetId : 'NULL'}\n"
        csvFiles['tweetsData'] << lineToWrite
        csvFiles['mentionsData'] << "${it + 100},login$randomMention1\n" +
                (randomMention1 != randomMention2 ? "${it + 100},login${randomMention2}\n" : "")
    }
    println '\'tweetsData.csv\' and \'mentions.csv\' data filling ended'
}

def retweetsDataThread = Thread.start {

    int randomRetweetId, randomRetweeterId
/**
 * checks whether retweetsData.csv contains row with such retweetId and retweeterId
 */
    def checkRetweetsFileContainsRowWithSuchPk = {
        int retweetId, int retweeterId ->
            csvFiles['retweetsData'].find {
                line -> line.matches ~"$retweetId,[0-9]+,login$retweeterId"
            } != null
    }
    println '\'retweetsData.csv\' data filling started...'
    retweetsCount.times {
        randomRetweetId = randomInt(tweetsCount)
        randomRetweeterId = randomInt(usersCount, false)
        while (checkRetweetsFileContainsRowWithSuchPk(randomRetweetId, randomRetweeterId)) {
            randomRetweetId = randomInt(tweetsCount)
            randomRetweeterId = randomInt(usersCount, false)
        }
        csvFiles['retweetsData'] << "${randomRetweetId},${randomInt()},login${randomRetweeterId}\n"
    }
    println '\'retweetsData.csv\' data filling ended'
}

def favouritesDataThread = Thread.start {

    /**
     * cheacks whether favouritesData.csv contains row with such tweetId and whoAddedToFavouritesId
     */
    def checkFavouritesFile = {
        int tweetId, int whoAddedToFavouritesId ->
            csvFiles['favouritesData'].find {
                line -> line.matches ~"$tweetId,login$whoAddedToFavouritesId"
            } != null
    }
    int randomTweetId, randomUserId
    println '\'favouritesData.csv\' data filling started...'
    favouritesCount.times {
        randomTweetId = randomInt(favouritesCount)
        randomUserId = randomInt usersCount, false
        while (checkFavouritesFile(randomTweetId, randomUserId)) {
            randomTweetId = randomInt(favouritesCount)
            randomUserId = randomInt usersCount, false
        }
        csvFiles['favouritesData'] << "$randomTweetId,login$randomUserId\n"
    }
    println '\'favouritesData.csv\' data filling ended'
}

usersDataThread.join()
tweetsAndMentionsDataThread.join()
retweetsDataThread.join()
favouritesDataThread.join()


println "files created for ${System.currentTimeMillis() - startTime} seconds"
println("checking tweetsData.csv for validity (tweet_id!=main_tweet_id)...")
def tweetsCsvValid = true
csvFiles['tweetsData'].readLines()[1..-1].eachWithIndex {
    curLine, index ->
        def tweetId = curLine[0..<curLine.indexOf(',')].toInteger()
        def mainTweetId = curLine[curLine.lastIndexOf(',') + 1..-1]
        if (mainTweetId.isInteger() && mainTweetId.toInteger() == tweetId) {
            println "invalid row #$index"
            tweetsCsvValid = false
        }
}
println "tweetsData.csv ${tweetsCsvValid ? 'valid' : 'invalid'}"