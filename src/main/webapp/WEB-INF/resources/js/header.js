$(document).ready(function () {

    $('.standard-logo img').attr('src', '/resources/images/logo.png');
    $('.retina-logo img').attr('src', '/resources/images/logo2x.png');

    $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        jqXHR.setRequestHeader(header, token);
    });

    $(".js-exit").click(function (event) {
        event.preventDefault();
        $('#js-exitForm').submit();
    });
    $('#js-user-search-form').submit(function (event) {
        event.preventDefault();
        window.location.pathname = '/' + $(this).children().val()
    })
});