$(document).ready(function () {

    var Popup = {
        current: null,

        open: function (el) {
            if (Popup.current) {
                $(Popup.current).hide();
            }
            $("#popup").show();
            $(el).show();
            Popup.current = el;
        },

        close: function () {
            if (Popup.current) {
                $(Popup.current).hide();
            }
            $("#popup").hide();
        }
    };
    $('#js-uploadAvatar').on("click", function () {
        Popup.open($("#popup-photo-upload"));
    });
    $("#js-photo").click(function () {
        Popup.open($("#popup-photo-upload"));
    });

    var photoUploadPopup = $("#popup-photo-upload");
    photoUploadPopup.find("button").click(function (event) {
        event.preventDefault();
        photoUploadPopup.find("input[type=file]").click();
    });
    $(".js-cancel, #popup-bg").click(function () {
        Popup.close($('#popup-photo-thumbnail'));
        Popup.close($("#popup-photo-upload"));
    });
    photoUploadPopup.find("input[type=file]").change(function () {
        /*photoUploadPopup.find("form").submit();*/  //загрузка фото перед сохранением для настройки
        Popup.close($("#popup-photo-upload"));
        /* Popup.open($("#popup-photo-thumbnail"));*/

        /*$('#js-buttonImgSafe').on("click", function () {*/
        var form = new FormData();
        var login = $('#js-login').val();
        var file = $('#js-changeImgUrl')[0].files[0];
        form.append('file', file);
        form.append('login', login);
        $.ajax({
            type: "POST",
            url: "/ajax/photo/change",
            processData: false,
            contentType: false,
            data: form,
            success: function (data) {
                $('#js-photo').find("img").attr("src", data);
                /*Popup.close($('#popup-photo-thumbnail'));*/
            }
        });
        /*})*/
    });


});