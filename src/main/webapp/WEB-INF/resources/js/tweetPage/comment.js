/**
 * Created by timur on 15.04.15.
 */
$(document).ready(function () {

    //functions
    {
        function updateAnswersCount() {
            $('.answersCount').text(parseInt($('.comment').length))
        }

        function removeAnswer(e) {
            if (e) {
                e.preventDefault()
            }
            var $answer = $(this).closest('.comment');
            $.post('/ajax/tweet/delete', {
                tweetId: $answer.attr('id')
            }).success(function () {
                $answer.hide(300, function () {
                    $answer.remove();
                    updateAnswersCount()
                })
            }).error(function (data) {
                console.error(data);
            });
        }
    }

    //initialize handlers
    {
        $('[name="add-answer-btn"]').click(function (e) {
            e.preventDefault();
            var $commentArea = $('#commentArea');
            $.post('/ajax/answer/add', {
                text: $commentArea.val(),
                mainTweetId: $('[name="mainTweetId"]').val()
            }).success(function (data) {
                $commentArea.val('');
                $('.commentlist')
                    .append(data)
                    .find('.comment:last .js-answer-remove')
                    .click(removeAnswer);
                updateAnswersCount()
            }).error(function (data) {
                console.error('error in adding answer');
                console.error(data);
            });
        });
        $('.js-answer-remove').click(removeAnswer);
        $('.js-retweet-add').click(function (e) {
            e.preventDefault();
            var icon = '<i class="icon-retweet"></i>';
            var $self = $(this);
            $.ajax({
                type: 'POST',
                url: '/ajax/retweet/add',
                contentType: "application/json",
                data: JSON.stringify({
                    tweetId: $self.closest('.entry').attr('id')
                }),
                success: function (data) {
                    $self.html(icon + (parseInt($self.text()) + 1));
                    $self.children().unwrap();
                    console.log('success');
                },
                error: function (data) {
                    console.error('error');
                    console.error(data);
                }
            });
        });

    }
});