<#assign security=JspTaglibs["http://www.springframework.org/security/tags"]>

<div id="top-bar">

    <div class="container clearfix">

        <div class="col_half nobottommargin clearfix">

            <!-- Top Links
            ============================================= -->
            <div class="top-links">
                <ul>
                    <li><a href="#">О проекте</a></li>
                    <li><a href="#">Контакты</a></li>
                <@security.authorize access='isAnonymous()'>
                    <li>
                        <a>Войти</a>

                        <div class="top-link-section">
                            <form method="post" action="/login" id="top-login" role="form">
                                <@security.csrfInput/>
                                <div class="input-group" id="top-login-username">
                                    <span class="input-group-addon"><i class="icon-user"></i></span>
                                    <input type="text" name="username" class="form-control" placeholder="Логин"
                                           required="">
                                </div>
                                <div class="input-group" id="top-login-password">
                                    <span class="input-group-addon"><i class="icon-key"></i></span>
                                    <input type="password" name="password" class="form-control" placeholder="Пароль"
                                           required="">
                                </div>
                            <#--<label class="checkbox">-->
                            <#--<input type="checkbox" value="remember-me"> Запомнить меня-->
                            <#--</label>-->
                                <button class="btn btn-danger btn-block" type="submit">Войти</button>
                            </form>
                        </div>
                    </li>
                </@security.authorize>
                </ul>
            </div>
            <!-- .top-links end -->

        </div>

        <div class="col_half fright col_last clearfix nobottommargin">

            <!-- Top Social
            ============================================= -->
            <div id="top-social">
                <ul>
                    <li>
                        <a href="#" class="si-bitbucket">
                            <span class="ts-icon">
                            <i class="icon-bitbucket"></i>
                            </span>
                            <span class="ts-text">Bitbucket</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="si-email3">
                            <span class="ts-icon">
                                <i class="icon-envelope-alt"></i>
                            </span>
                            <span class="ts-text">aronwest001@gmail.com</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="si-email3">
                            <span class="ts-icon">
                                <i class="icon-envelope-alt"></i>
                            </span>
                            <span class="ts-text">altdilia@gmail.com</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="si-email3">
                            <span class="ts-icon">
                                <i class="icon-envelope-alt"></i>
                            </span>
                            <span class="ts-text">arthurcots@gmail.com</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #top-social end -->

        </div>

    </div>

</div>
<!-- #top-bar end -->

<!-- Header
============================================= -->
<header id="header" class="full-header">

    <div id="header-wrap">

        <div class="container clearfix">

        <@security.authorize access="isAuthenticated()">
            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
        </@security.authorize>

            <!-- Logo
            ============================================= -->
            <div id="logo">
                <a href="/" class="standard-logo">
                    <img <#--src="/resources/images/logo.png"--> alt="Твиттер">
                </a>
                <a href="/" class="retina-logo">
                    <img <#--src="/resources/images/logo2x.png"--> alt="Твиттер">
                </a>
            </div>
            <!-- #logo end -->

            <!-- Primary Navigation
            ============================================= -->
            <nav id="primary-menu">
            <@security.authorize access="isAuthenticated()">

                <ul class="js-toplinks">
                    <li>
                        <a href="/">
                            <div>Лента</div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div>Упоминания</div>
                        </a>
                    </li>
                    <li>
                        <a href="/favourites">
                            <div>Избранное</div>
                        </a>
                    </li>
                    <@security.authentication property="principal.login" var='principal' scope='page'/>
                    <li class="current">
                        <a href="/${principal}">
                            <div>${principal}</div>
                        </a>
                        <ul>
                            <li>
                                <a href="/editProfile">Настройки</a>
                            </li>
                            <li>
                                <a href="#" class="js-exit">Выйти
                                    <form id="js-exitForm" action="/logout" method="post">
                                        <@security.csrfInput/>
                                        <button style="display: none"
                                                type="submit">
                                        </button>
                                    </form>
                                </a>
                            </li>
                        </ul>

                    </li>
                </ul>
            </@security.authorize>

                <!-- Top Cart
                ============================================= -->
                <!-- #top-cart end -->

                <!-- Top Search
                ============================================= -->
                <div id="top-search">
                    <a href="#" id="top-search-trigger">
                        <i class="icon-search3"></i>
                        <i class="icon-line-cross"></i>
                    </a>

                    <form method="get" id="js-user-search-form">
                        <input type="text" name="login" class="form-control"
                               placeholder="Введите логин пользователя" autocomplete="off">
                    </form>
                </div>
                <!-- #top-search end -->

            </nav>
            <!-- #primary-menu end -->

        </div>

    </div>

</header>