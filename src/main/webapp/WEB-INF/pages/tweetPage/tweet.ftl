<#assign security=JspTaglibs['http://www.springframework.org/security/tags']>
<#include '../template.ftl'>
<@security.authorize access='isAnonymous()'>
    <@security.authentication property='principal' var='principal' scope='page'/>
</@security.authorize>
<@security.authorize access="isAuthenticated()">
    <@security.authentication property='principal.login' var='principal' scope='page'/>
</@security.authorize>
<#function alreadyDidRetweet retweets>
    <#list retweets as retweet>
        <#if retweet.creator == principal>
            <#return true>
        </#if>
    </#list>
    <#return false>
</#function>
<#macro body>
    <#assign creator = tweet.creator.login>
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="single-post nobottommargin">
                <div class="entry clearfix" id="${tweet.tweetId}">
                    <div class="entry-title">
                        <h2><a href="/${creator}">${creator}</a></h2>
                    </div>
                    <ul class="entry-meta clearfix">
                        <li>
                            <i class="icon-calendar3"></i> <#--${(tweet.time?number_to_datetime?string)!'хз'}-->${tweet.timeView}
                        </li>
                        <li><i class="icon-user"></i>${(tweet.mentionedUsers?size)!0}</li>
                        <li>
                            <#if principal='anonymousUser' || creator == principal || alreadyDidRetweet(tweet.retweets)>
                                <i class="icon-retweet"></i>${(tweet.retweets?size)!0}
                            <#else>
                                <a href="#" class="js-retweet-add"><i
                                        class="icon-retweet"></i>${(tweet.retweets?size)!0}</a>
                            </#if>
                        </li>
                        <li><i class="icon-star"></i>${(tweet.whoAddedToFavourites?size)!0}</li>
                        <li></li>
                    </ul>
                    <div class="entry-content notopmargin">
                        <div class="author-image">
                            <a href="/${creator}">
                                <img class="img-rounded" src="${tweet.creator.avatarURL!defPic}" alt="${creator}">
                            </a>
                        </div>
                        <p>${tweet.text}</p>
                    </div>
                </div>
                <div id="comments" class="clearfix">
                    <h3 id="comments-title">
                        <span class="answersCount">${(tweet.answers?size)!0}</span> Комментариев
                    </h3>
                    <ol class="commentlist clearfix">
                        <#list tweet.answers as answer>
                            <#include 'commentInclude.ftl'>
                        </#list>
                    </ol>
                    <div class="clear"></div>
                    <@security.authorize access='isAuthenticated()'>
                        <div id="respond" class="clearfix">
                            <h4>Оставьте <span>Комментарий</span></h4>

                            <form class="clearfix" action="#" method="post" id="commentform">
                                <div class="clear"></div>
                                <input type="hidden" name="mainTweetId" value="${tweet.tweetId}">

                                <div class="col_full">
                                    <label for="commentArea">Comment</label>
                                <textarea id="commentArea" rows="3" maxlength="140" tabindex="4"
                                          style="resize: none"
                                          class="sm-form-control"></textarea>
                                </div>
                                <div class="col_full nobottommargin">
                                    <button name="add-answer-btn" type="submit" id="submit-button" tabindex="5"
                                            value="Submit"
                                            class="button button-3d nomargin">Ответить
                                    </button>
                                </div>
                            </form>
                        </div>
                    </@security.authorize>
                </div>
            </div>
        </div>
    </div>
</section>
</#macro>
<@main title='Твит от ${tweet.creator.login}'
customScripts=["/resources/js/tweetPage/comment.js"]/>