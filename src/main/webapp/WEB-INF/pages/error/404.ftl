<#include "../template.ftl">
<#assign security=JspTaglibs["http://www.springframework.org/security/tags"]>
<#macro body>
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <div class="col_half nobottommargin">
                <div class="error404 center">404</div>
            </div>

            <div class="col_half nobottommargin col_last">

                <div class="heading-block nobottomborder">
                    <h4>Страницы, которую вы ищете, не существует</h4>
                <#--<span>Try searching for the best match or browse the links below:</span>-->
                </div>

            <#--<form action="#" method="get" role="form" class="nobottommargin">-->
            <#--<div class="input-group input-group-lg">-->
            <#--<input type="text" class="form-control" placeholder="Search for Pages...">-->
            <#--<span class="input-group-btn">-->
            <#--<button class="btn btn-danger" type="button">Поиск</button>-->
            <#--</span>-->
            <#--</div>-->
            <#--</form>-->

            </div>

        </div>

    </div>

</section>
</#macro>
<@main title="404"/>