<#include "../template.ftl">
<#macro body>
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div id="posts" class="post-grid grid-2 clearfix">
                <#include "../postsInclude.ftl">
            </div>
        </div>
    </div>
</section>
</#macro>
<@main title="${user.login!'Профиль'}"
customStyles=["/resources/css/tweet.css"]
customScripts=[
"/resources/js/tweets.js",
'/resources/js/retweets.js']/>