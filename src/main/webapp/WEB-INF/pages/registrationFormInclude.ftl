<#assign security=JspTaglibs["http://www.springframework.org/security/tags"]>
<#assign form=JspTaglibs["http://www.springframework.org/tags/form"]>
<#assign spring=JspTaglibs["http://www.springframework.org/tags"]>
<@form.form id="register-form" modelAttribute="userDTO" action="/register" method="post">
    <@security.csrfInput/>
<div class="col_half">
    <label for="firstname">Имя:</label>
    <@form.input id="firstname" path='firstname' cssClass='required form-control input-block-level'/>
    <@form.errors path="firstname"/>
</div>
<div class="col_half col_last">
    <label for="lastname">Фамилия:</label>
    <@form.input id="lastname" path='lastname' cssClass='required form-control input-block-level'/>
    <@form.errors path="lastname"/>
</div>
<div class="clear"></div>

<div class="col_half">
    <label for="email">Email:</label>
    <@form.input path="email" id="email" cssClass="required form-control input-block-level"/>
    <@form.errors path="email"/>
</div>

<div class="col_half col_last">
    <label for="login">Выберите логин:</label>
    <@form.input path="login" id="login" cssClass="required form-control input-block-level"/>
    <@form.errors path="login"/>
</div>

<div class="clear"></div>

<div class="col_half">
    <label for="password">Пароль:</label>
    <@form.password path="password" id="password" cssClass="required form-control input-block-level"/>
    <@form.errors path="password"/>
</div>
<div class="col_half col_last">
    <label for="repassword">Повторите пароль:</label>
    <@form.password id="repassword" path="repassword" cssClass="required form-control input-block-level"/>
    <@form.errors path="repassword"/>
</div>

<div class="clear"></div>

<div class="col_full nobottommargin">
    <button class="button button-3d button-black nomargin" id="register-form-submit"
            name="register-form-submit" value="register">Зарегистрироваться
    </button>
</div>
</@form.form>