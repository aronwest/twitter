<#assign security=JspTaglibs["http://www.springframework.org/security/tags"]>
<#assign form=JspTaglibs["http://www.springframework.org/tags/form"]>
<#assign spring=JspTaglibs["http://www.springframework.org/tags"]>
<#include "template.ftl">
<#macro body>

<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <div class="col_one_third nobottommargin">

                <div class="well well-lg nobottommargin">
                    <form id="login-form" name="login-form" class="nobottommargin" action="/login" method="post">
                        <@security.csrfInput/>
                        <h3>Войти в свой аккаунт</h3>

                        <div class="col_full">
                            <label for="login-form-username">Логин:</label>
                            <input type="text" id="login-form-username" name="username"
                                   class="required form-control input-block-level"/>
                        </div>
                        <div class="col_full">
                            <label for="login-form-password">Пароль:</label>
                            <input type="password" id="login-form-password" name="password"
                                   class="required form-control input-block-level"/>
                        </div>
                        <div class="col_full nobottommargin">
                            <button type="submit" class="button button-3d nomargin" id="login-form-submit"
                                    name="login-form-submit">Войти
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col_two_third col_last nobottommargin">
                <div class="well well-lg">
                    <h3>Все еще нет аккаунта? Зарегистрируйтесь!</h3>
                    <#include "registrationFormInclude.ftl"/>
                </div>
            </div>
        </div>
    </div>
</section>
</#macro>
<@main customStyles=[
"/resources/css/login.css"]
title="Твиттер"/>