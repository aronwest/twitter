<#assign security=JspTaglibs["http://www.springframework.org/security/tags"]>
<#assign form=JspTaglibs["http://www.springframework.org/tags/form"]>
<#assign spring=JspTaglibs["http://www.springframework.org/tags"]>
<#include "template.ftl">
<#macro body>
<button id="js-uploadAvatar">Загрузить фото</button>
<div class="photo" id="js-photo">

    <img src="${user.avatarURL!defPic}" alt="Фамилия Имя" width="220" height="220">

</div>

<input type="hidden" id="js-login" value="${user.login}">
<div class="popup" id="popup" style="display:none;">
    <div class="background" id="popup-bg"></div>
    <div class="window photo-upload" id="popup-photo-upload" style="display:none;">
        <form action="#" method="post">
            <div class="heading">
                <h3>Загрузка новой фотографии</h3>
            </div>
            <div class="center">
                <div>Вы можете загрузить изображение в формате JPG, GIF или PNG.</div>
                <input type="file" id="js-changeImgUrl" name="file">
                <button tabindex="1">Выбрать файл</button>
            </div>


        </form>
    </div>
</div>
</#macro>
<@main
customStyles=["/resources/css/admin.css", "/resources/css/profile.css"]
customScripts=["/resources/js/uploadImage.js"]
title="Твиттер"/>