<#include "../template.ftl">
<#macro body>
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div id="createTweetPopup">
                <#include "createTweetInclude.ftl">
            </div>
            <!-- Posts -->
            <div id="posts" class="post-grid grid-2 clearfix">
                <#include "../postsInclude.ftl">
            </div>
            <!-- #posts end -->
        </div>
    </div>
</section>
</#macro>
<#assign js_location='/resources/js'/>
<@main title="Лента" customStyles= [
"/resources/css/tweet.css"
]
customScripts=[
"${js_location}/tape.js",
'${js_location}/tweets.js',
'${js_location}/retweets.js',
'${js_location}/favourites.js'
]/>