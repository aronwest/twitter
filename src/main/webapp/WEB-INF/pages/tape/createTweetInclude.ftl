<#assign security=JspTaglibs["http://www.springframework.org/security/tags"]>
<#assign form=JspTaglibs["http://www.springframework.org/tags/form"]>
<#assign spring=JspTaglibs["http://www.springframework.org/tags"]>
<div class="modal fade" id="addTweet" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <label for="tweet">Текст твита:</label>
                    <textarea id="tweet" rows="5" cols="30" class="js-text required input-block-level"
                              name="text" maxlength='140' style="resize: none"></textarea>
            </div>
            <div class="modal-footer">
                <div id="charCountLabel" class="col_one_sixth nomargin"></div>
                <div class="col_five_sixth col_last nomargin">
                    <button class="btn btn-default btn-lg" id="js-sendTweet"
                            value="Отправить твит" disabled>Отправить твит
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>