package service;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import ru.kpfu.itis.model.Tweet;
import ru.kpfu.itis.repository.RetweetRepository;
import ru.kpfu.itis.repository.TweetRepository;
import ru.kpfu.itis.repository.UserRepository;
import ru.kpfu.itis.service.TweetService;
import ru.kpfu.itis.service.implementation.TweetServiceImpl;

import java.util.concurrent.ExecutorService;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(MockitoJUnitRunner.class)
public class TweetServiceTest extends TestCase {

    @Mock
    ExecutorService executorService;
    @Mock
    UserRepository userRepository;
    @Mock
    RetweetRepository retweetRepository;
    //    private TweetService tweetService;
//    @Mock
//    private Retweet retweet;
//    @Mock
//    private Tweet tweet;
    @InjectMocks
    TweetService tweetService = new TweetServiceImpl();
    @Mock
    private TweetRepository tweetRepository;

//    @Before
//    public void setUp() throws Exception {
//
////        tweetService = new TweetServiceImpl(tweetRepository,
////                userRepository, retweetRepository);
////        retweet = mock(Retweet.class);
////        tweet = mock(Tweet.class);
//    }
/*
//    @Before
//    public void doSetup() {
//        tweetRepository = mock(TweetRepository.class);
//        tweetService = new TweetServiceImpl(tweetRepository, userRepository, retweetRepository);
//    }

    public void testFindAllTweetsByUserAndSort() throws Exception {

    }

    private static Tweet createTweet() {
        Tweet tweet = new Tweet();
        return tweet;
    }
//    private static Retweet createRetweet() {
//        Retweet retweet = new Retweet();
//        retweet.setCreator("Homer");
//        return retweet;
//    }

    @Test
    public void testRetweetSave() throws Exception {
//        when(retweet.setCreator()).thenReturn("Homer");
//        Retweet retweet = createRetweet();
//        retweet.setCreator();
        tweetService.save(retweet); // when
        Mockito.verify(retweetRepository, Mockito.times(1)).save(retweet); // then

        retweet.setTweet(null);
        if (retweet.getTweet() == null) {
            Mockito.doReturn(createTweet()).when(tweetRepository).findOne(Mockito.anyLong());
            tweetRepository.findOne(Mockito.anyLong());
//            tweet.getRetweets().add(retweet);
//            retweet.setTweet(tweet);
        }
    }

    private static TweetDTO createTweetDTO() {
        TweetDTO tweetDTO = new TweetDTO();
        tweetDTO.setText("@Homer @Bart @Lisa Hello, gyus!");
        return tweetDTO;
    }



    @Test
    public void testTweetSave() throws Exception {
//        Tweet tweet = new Tweet();
        String text = createTweetDTO().getText();
//        List<String> mentionedUsersLogins = new ArrayList<>();
//        Matcher matcher = Pattern.compile("([\\W_&&[^@]]+|^)@([A-Za-z0-9]+)").matcher(text);
//        while (matcher.find()) {
//            mentionedUsersLogins.add(matcher.group(2));
//        }
//        List<User> mentionedUsers = null;
//        if (mentionedUsersLogins.size() != 0) {
//            mentionedUsers = userRepository.findByLoginIn(mentionedUsersLogins);
//        }
//        tweet.setMentionedUsers(mentionedUsers);
        assertEquals(3, tweet.getMentionedUsers().size());
    }

    public void testSave2() throws Exception {

    }

    public void testDeleteTweet() throws Exception {

    }


    @Test
    public void testDeleteRetweet() throws Exception {
//        RetweetId retweetId = Mockito.mock(RetweetId.class);
//        verify(retweetId).setCreator("Homer");
//        verify(retweetId).setTweetId(Mockito.anyLong());
        verify(retweetRepository, times(1)).delete(Mockito.any(RetweetId.class));
    }

//    public void update(Long id, String text)
   /* @Test
    public void testUpdate() throws Exception {
        verify(tweetRepository, times(1)).updateTweetText(Mockito.anyLong(), Mockito.anyString());
    }*/

   /* public void testGetTweetsPage() throws Exception {

    }


    */

    private static Tweet createTweet() {
        Tweet tweet = new Tweet();
        return tweet;
    }

    @Test
    public void testFindById() throws Exception {
        Mockito.doReturn(createTweet()).when(tweetRepository).findOne(Mockito.anyLong());
        // when
        final Tweet tweet = tweetService.findById(Mockito.anyLong());
        // then
        assertThat(tweet).isNotNull();
    }
    /*
    public void testFindTweetsAccessedTo() throws Exception {

    }

    public void testTweetsAndRetweetsAccessedTo() throws Exception {

    }*/

}